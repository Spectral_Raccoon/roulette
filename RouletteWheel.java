import java.util.Random;
public class RouletteWheel {
    private Random rand;
    private int number;

    public RouletteWheel() {
        this.rand = new Random();
        this.number = 0;
    }

    public int spin() {
        int max = 37;
        this.number = rand.nextInt(max) - 1;
        return this.number;
    }

    public int getValue() {
        return this.number;
    }
}