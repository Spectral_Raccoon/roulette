import java.util.Scanner;
public class Roulette {
    public static void main(String[] args) {
        Roulette obj = new Roulette();
        obj.neverTellMeTheOdds();
    }

    public int neverTellMeTheOdds() {
        int winnings = 0;
        Scanner reader = new Scanner(System.in);
        System.out.println("Would you like to place a bet? (y or n)");
        String response = reader.nextLine();
        if (response.equals("y") || response.equals("Y") || response.equals("yes")) {
            System.out.println("What number would you like to bet on? (between 0-36)");
            int bet = reader.nextInt();
            System.out.println("How much would you like to bet?");
            int am_bet = reader.nextInt();
            reader.close();
            spinningTheWheel();
            if (bet == spinningTheWheel()) {
                winnings = am_bet*35;
                System.out.println("Nice bet!: " + winnings);
            } else {
                winnings = 0;
                System.out.println("Sorry about this: " + winnings);
            }
            return winnings;
        } else {
            System.out.println("Sorry to see you go, we would've really liked to take your money...");
        }
        reader.close();
        return winnings;
    }

    public int spinningTheWheel() {
        RouletteWheel theOdds = new RouletteWheel();
        return theOdds.spin();
    }
}